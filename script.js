 //Preload light images
 var imgName = 'light/ezgif-frame-';
 var imgExt = '.png';
 var $ = document.querySelector.bind(document)
 var intro_play = $('.intro-play')
//  var video = $('#vid')
 var body = $('body')
 var main = $('#main')
 var light = $('.light')

 function preloadUrl(url) {
     var s = document.createElement('link');
     s.rel = 'preload';
     s.as = 'image';
     s.href = url;
     var x = document.getElementsByTagName('head')[0];
     x.appendChild(s);
 }

 // Load sẵn hình
 for (var i = 1; i < 122; i++) {
     var url = imgName + i + imgExt;
     preloadUrl(url);
 }


 // Anim
 var number = 121,
     timer = 12000,
     duration = timer / number,
     count = 1;
 var lightInterval;


 var isClick = false;
 var isPlay = false;

 function animLight() {

     lightInterval = setInterval(function () {
         if (isClick) {
             if (count > number) {
                 // End video 
                 
                 // Nếu muốn kết thúc clear lightInterval và add class light-hide
                 clearInterval(lightInterval);
                 document.querySelector('.light').classList.add('light-hide');
                 body.classList.add('loaded')
                 light.classList.add('anim-fade-out');
                 main.classList.add('anim-fade-in');
                 // Nếu muốn loop sét lại count = 1
                //  count = 1;


             }

         } else {
             if (count > 24) {
                 count = 1;
             }
         }

         lightImg.src = (imgName + count + imgExt);
         count++;

     }, duration);

 }

 startLight.addEventListener("mouseover", function (event) {
     if (!isPlay) {
         // Start loop at 8 --> 24 -- > 1
         count = 8;
         animLight();
         isPlay = true;
     }

 }, false);

 startLight.addEventListener('click', function () {
     isClick = true;
     document.querySelector('.light').classList.add('light-playing');
     if (!isPlay) {
         animLight();
     }
 });